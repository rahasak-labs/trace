package com.score.aplos.config

import com.typesafe.config.ConfigFactory

trait CassandraConf {
  val cassandraConf = ConfigFactory.load("cassandra.conf")

  // cassandra config
  lazy val cassandraKeyspace = cassandraConf.getString("cassandra.keyspace")
  lazy val cassandraHosts = cassandraConf.getString("cassandra.hosts").split(",").toSeq
  lazy val cassandraPort = cassandraConf.getInt("cassandra.port")
  lazy val cassandraKeyspaceSchema = cassandraConf.getString("cassandra.keyspace-schema")
  lazy val preparedStatementCacheSize = cassandraConf.getInt("cassandra.preparedStatementCacheSize")
}
