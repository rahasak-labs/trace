package com.score.aplos.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

/**
  * Load configurations define in application.conf from here
  *
  * @author eranga herath(erangaeb@gmail.com)
  */
trait AppConf {

  // config object
  val appConf = ConfigFactory.load()

  // senzie config
  lazy val serviceMode = Try(appConf.getString("service.mode")).getOrElse("DEV")
  lazy val serviceName = Try(appConf.getString("service.name")).getOrElse("aplos")
  lazy val servicePort = Try(appConf.getInt("service.port")).getOrElse(8761)

  // keys config
  lazy val keysDir = Try(appConf.getString("keys.dir")).getOrElse(".keys")
  lazy val publicKeyLocation = Try(appConf.getString("keys.public-key-location")).getOrElse(".keys/id_rsa.pub")
  lazy val privateKeyLocation = Try(appConf.getString("keys.private-key-location")).getOrElse(".keys/id_rsa")

}
