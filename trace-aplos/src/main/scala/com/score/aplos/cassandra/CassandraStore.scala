package com.score.aplos.cassandra

import com.dataoperandz.cassper.Cassper

import scala.concurrent.Await
import scala.concurrent.duration._

object CassandraStore extends VoteStore with CassandraCluster {

  def init() = {
    // create keyspace
    session.execute(cassandraKeyspaceSchema)

    // migration via cassper
    // create udts, tables
    val builder = new Cassper().build("rahasak", session)
    builder.migrate("rahasak")
  }

  def createVote(vote: Vote) = {
    Await.result(createVoteAsync(vote), 10.seconds)
  }

  def updateVote(id: String, vote: PartyVote) = {
    Await.result(updateVoteAsync(id, vote), 10.seconds)
  }

  def getVote(id: String) = {
    Await.result(searchVoteAsync(id), 10.seconds).headOption
  }

}
