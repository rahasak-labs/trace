package com.score.aplos.cassandra

import com.datastax.driver.core.{Cluster, _}
import io.getquill.context.cassandra.CassandraSessionContext

import scala.collection.JavaConverters._

trait UdtMapper {

  this: CassandraSessionContext[_] =>

  var cluster: Cluster
  var keySpace: String

  implicit val locationDecoder: Decoder[Location] = {
    decoder((index, row) => {
      val a = row.getUDTValue(index)
      Location(a.getDouble("lat"), a.getDouble("lon"))
    })
  }

  implicit val locationEncoder: Encoder[Location] = {
    encoder((index, location, row) => {
      val tpe = cluster.getMetadata.getKeyspace("rahasak").getUserType("geo_point")
      val udt = tpe.newValue()
        .setDouble("lat", location.lat)
        .setDouble("lon", location.lon)
      row.setUDTValue(index, udt)
    })
  }

  implicit val contactDecoder: Decoder[Contact] = {
    decoder((index, row) => {
      val a = row.getUDTValue(index)
      Contact(a.getString("name"), a.getString("email"), a.getString("phone"), a.getString("address"))
    })
  }

  implicit val contactEncoder: Encoder[Contact] = {
    encoder((index, contact, row) => {
      val tpe = cluster.getMetadata.getKeyspace("rahasak").getUserType("contact")
      val udt = tpe.newValue()
        .setString("name", contact.name)
        .setString("email", contact.email)
        .setString("phone", contact.phone)
        .setString("address", contact.address)
      row.setUDTValue(index, udt)
    })
  }

  implicit val contactsDecoder: Decoder[Set[Contact]] = {
    decoder((index, row) => {
      row.getSet(index, classOf[UDTValue])
        .asScala
        .map(s => Contact(s.getString("name"), s.getString("email"), s.getString("phone"), s.getString("address"))).toSet
    })
  }

  implicit val contactsEncoder: Encoder[Set[Contact]] = {
    encoder((index, contacts, row) => {
      val tpe = cluster.getMetadata.getKeyspace("rahasak").getUserType("contact")
      val udt = contacts.map { contact =>
        // set contact data
        val uv = tpe.newValue()
          .setString("name", contact.name)
          .setString("email", contact.email)
          .setString("phone", contact.phone)
          .setString("address", contact.address)
        uv
      }.asJava
      row.setSet(index, udt)
    })
  }

}

