package com.score.aplos.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper
import com.datastax.driver.core.LocalDate

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}

case class Location(lat: Double, lon: Double)

case class Contact(name: String,
                   email: String,
                   phone: String,
                   address: String)

case class Electorate(
                       province: String,
                       district: String,
                       electorate: String,
                       asanaya: String,
                       kottasaya: String,
                       dsDivision: String,
                       gsDivision: String,
                       location: Location
                     )

case class PartyVote(
                      partyName: Option[String] = None,
                      partyVotes: Option[Long] = None
                    )

case class Vote(
                 id: String,

                 province: String,
                 district: String,
                 electorate: String,
                 asanaya: String,
                 kottasaya: String,
                 dsDivision: String,
                 gsDivision: String,
                 location: Location,

                 electionYear: String,
                 electionDate: Option[LocalDate],

                 partyName: Option[String] = None,
                 totalCases: Option[Long] = None,
                 totalVaccination: Option[Long] = None,

                 timestamp: Date = new Date()
               )

trait VoteStore extends CassandraCluster {

  import ctx._

  def createVoteAsync(electionResult: Vote): Future[Unit] = {
    val q = quote {
      query[Vote].insert(lift(electionResult))
    }
    ctx.run(q)
  }

  def updateVoteAsync(id: String, vote: PartyVote): Future[Unit] = {
    val q = quote {
      query[Vote]
        .filter(r => r.id == lift(id))
        .update(
          _.partyName -> lift(vote.partyName),
          _.totalCases -> lift(vote.partyVotes)
        )
    }
    ctx.run(q)
  }

  def searchVoteAsync(id: String): Future[List[Vote]] = {
    val q = quote {
      query[Vote]
        .filter(r => r.id == lift(id))
        .take(1)
    }
    ctx.run(q)
  }

}

import scala.concurrent.duration._

//object M5 extends App with ProjectStore {
//  // create document
//  val pro = Project(
//    "2323",
//    "wewe",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    Location(3.1, 3.2),
//    Set(Party("1", "1", "a"), Party("2", "2", "a")),
//    Set()
//  )
//
//  val house1 = House(
//    "2323",
//    "wewe",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    Location(3.1, 3.2),
//    "sdsfd",
//    "sdsfd",
//    "sdsfd",
//    4,
//    "sdsfd",
//    "11111111111",
//    "22222222222",
//    "sdsfd",
//    "sdsfd"
//  )
//  Await.result(createProjectAsync(pro), 10.seconds)
//
//  val hs = Await.result(searchProjectAsync("2323"), 10.seconds)
//  println(hs)
//
//  Await.result(updateContractorsAsync("2323", Set(Party("hooo", "hooo", "java"))), 10.seconds)
//
//  val hs1 = Await.result(searchProjectAsync("2323"), 10.seconds)
//  println(hs1)
//}
