package com.score.aplos.util

import ch.qos.logback.classic.{Level, Logger}
import com.score.aplos.config.AppConf
import org.slf4j

object LoggerFactory extends AppConf {
  def init(): Unit = {
    setupLogging()
  }

  def setupLogging(): Unit = {
    val rootLogger = slf4j.LoggerFactory.getLogger("root").asInstanceOf[Logger]

    serviceMode match {
      case "DEV" =>
        rootLogger.setLevel(Level.DEBUG)
      case "PROD" =>
        rootLogger.setLevel(Level.INFO)
      case _ =>
        rootLogger.setLevel(Level.INFO)
    }
  }
}
