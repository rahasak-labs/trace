package com.score.aplos

import akka.actor.ActorSystem
import com.score.aplos.actor.VoteFlyActor
import com.score.aplos.actor.VoteFlyActor.VoteFly
import com.score.aplos.cassandra.CassandraStore
import com.score.aplos.config.{AppConf, FeatureToggleConf}
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.util.LoggerFactory

object Main extends App with AppConf with FeatureToggleConf {

  // setup logging
  LoggerFactory.init()

  // actor systems
  implicit val system = ActorSystem.create("rahasak")

  // create schema/indexes
  CassandraStore.init()
  ElasticStore.init()

  // fly if feature toggle enabled
  if (useFly) {
    system.actorOf(VoteFlyActor.props(), name = "FlyActor") ! VoteFly
  }

}

