package com.score.aplos.elastic

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.{GET, PUT}
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, StatusCodes}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import com.score.aplos.config.{CassandraConf, ElasticConf}
import com.score.aplos.util.AppLogger
import org.apache.lucene.search.join.ScoreMode
import org.elasticsearch.common.unit.DistanceUnit
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.sort.SortOrder

import scala.concurrent.Await
import scala.concurrent.duration._

object ElasticStore extends ElasticCluster with AppLogger with CassandraConf with ElasticConf {

  case class Sort(field: String, ascending: Boolean)

  case class Criteria(name: String, value: String, mustNot: Boolean = false, should: Boolean = false)

  case class Nested(path: String, criterias: List[Criteria], mustNot: Boolean = false)

  case class GeoFilter(name: String, distance: String, lat: Double, lon: Double)

  case class Range(name: String, from: String, to: String)

  case class Page(offset: Int, limit: Int)

  def init()(implicit system: ActorSystem): Unit = {
    System.setProperty("es.set.netty.runtime.available.processors", "false")

    // create offers index with custom mapping for location(geo_point)
    val disc = "^((?!location).*)"
    val props =
      """
        |"location": {
        |  "type": "geo_point",
        |  "cql_collection": "singleton"
        |}
      """.stripMargin

    initIndex(voteElasticIndex, voteElasticDocType, Some(disc), Some(props))
  }

  def initIndex(index: String, docTyp: String, disc: Option[String] = None, props: Option[String] = None)(implicit system: ActorSystem): Unit = {
    implicit val ec = system.dispatcher
    implicit val materializer = ActorMaterializer()
    implicit val timeout = 40.seconds

    // params
    val uri = s"http://${elasticHosts.head}:9200/$index"
    val json =
      s"""
      {
        "settings":{
          "keyspace": "$cassandraKeyspace"
        },
        "mappings": {
          "$docTyp" : {
            "discover" : "${disc.getOrElse(".*")}",
            "properties": {
              ${props.getOrElse("")}
            }
          }
        }
      }
    """

    // check index exists
    val get = HttpRequest(GET, uri = uri)
    Await.result(Http().singleRequest(get), timeout).status match {
      case StatusCodes.NotFound =>
        // index not exists
        logger.info(s"init index request uri $uri json $json")

        // create index
        val put = HttpRequest(PUT, uri = uri).withEntity(ContentTypes.`application/json`, ByteString(json.stripLineEnd))
        val resp = Await.result(Http().singleRequest(put), timeout)

        logger.info(s"init index response: $resp")
      case _ =>
        // index already exists
        logger.info(s"$index index already exists")
    }
  }

  def buildSearch(index: String, docType: String, terms: List[Criteria], wildcards: List[Criteria],
                  mustFilters: List[Criteria], shouldFilters: List[Criteria], nested: Option[Nested],
                  geoFilter: Option[GeoFilter], sorts: List[Sort], page: Option[Page]) = {
    // term query to search on arrays
    val termBuilder = QueryBuilders.boolQuery()
    terms.filter(_.value.nonEmpty).foreach { f =>
      termBuilder.must(QueryBuilders.termQuery(f.name, f.value))
    }

    // wildcard
    val wildcardBuilder = QueryBuilders.boolQuery()
    wildcards.filter(_.value.nonEmpty).foreach { f =>
      wildcardBuilder.should(QueryBuilders.wildcardQuery(f.name, f.value))
    }

    // must filter
    val mustBuilder = QueryBuilders.boolQuery()
    mustFilters.filter(_.value.nonEmpty).foreach { f =>
      if (f.mustNot)
        mustBuilder.mustNot(QueryBuilders.matchQuery(f.name, f.value))
      else
        mustBuilder.must(QueryBuilders.matchQuery(f.name, f.value))
    }

    // should filter
    val shouldBuilder = QueryBuilders.boolQuery()
    shouldFilters.filter(_.value.nonEmpty).foreach { f =>
      shouldBuilder.should(QueryBuilders.matchQuery(f.name, f.value))
    }

    // combine builders with nested
    val builder = QueryBuilders.boolQuery()
    nested match {
      case Some(n) =>
        val nestedFilters = QueryBuilders.boolQuery()
        n.criterias.foreach { f =>
          nestedFilters.must(QueryBuilders.matchQuery(f.name, f.value))
        }

        // nested builder
        val nestedBuilder = QueryBuilders.boolQuery()
        if (n.mustNot)
          nestedBuilder.mustNot(QueryBuilders.nestedQuery(n.path, nestedFilters, ScoreMode.None))
        else
          nestedBuilder.must(QueryBuilders.nestedQuery(n.path, nestedFilters, ScoreMode.None))

        // combine all builders
        builder.must(termBuilder).must(nestedBuilder).must(mustBuilder).must(shouldBuilder).must(wildcardBuilder)
      case None =>
        // combine all builders
        builder.must(termBuilder).must(mustBuilder).must(shouldBuilder).must(wildcardBuilder)
    }

    // geo distance filter
    geoFilter match {
      case Some(gf) =>
        val filter = QueryBuilders.geoDistanceQuery(gf.name)
          .point(gf.lat, gf.lon)
          .distance(gf.distance, DistanceUnit.KILOMETERS)
        builder.must(filter)
      case _ =>
    }

    // build search
    val searchBuilder = client
      .prepareSearch(index)
      .setTypes(docType)
      .setQuery(builder)

    // add sorts
    sorts.foreach { s =>
      searchBuilder.addSort(s.field, if (s.ascending) SortOrder.ASC else SortOrder.DESC)
    }

    // build pagination
    val p = page.getOrElse(Page(0, 10))
    searchBuilder.setFrom(p.offset).setSize(p.limit)
  }

  def dehydrate(value: String): String = {
    if (value == null) "" else value
  }

}

