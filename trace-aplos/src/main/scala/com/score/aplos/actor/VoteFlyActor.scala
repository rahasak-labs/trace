package com.score.aplos.actor

import java.util.UUID

import akka.actor.{Actor, Props}
import com.score.aplos.cassandra._
import com.score.aplos.config.AppConf
import com.score.aplos.util.{AppLogger, DateFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.util.Random


object VoteFlyActor {

  case class VoteFly()

  val electorates = readElectorate

  val years = List("2000", "2005", "2010", "2015", "2020")
  val dates = List("2000-08-29", "2005-07-10", "2010-03-07", "2015-01-10", "2020-10-15")

  val p11 = PartyVote(Option("PARTY1"), Option(1100))
  val p12 = PartyVote(Option("PARTY1"), Option(1500))
  val p13 = PartyVote(Option("PARTY1"), Option(1400))
  val p14 = PartyVote(Option("PARTY1"), Option(1300))
  val p15 = PartyVote(Option("PARTY1"), Option(1200))
  val p16 = PartyVote(Option("PARTY1"), Option(1250))
  val p17 = PartyVote(Option("PARTY1"), Option(850))
  val p18 = PartyVote(Option("PARTY1"), Option(750))
  val p19 = PartyVote(Option("PARTY1"), Option(1290))
  val p110 = PartyVote(Option("PARTY1"), Option(690))

  val p21 = PartyVote(Option("PARTY2"), Option(880))
  val p22 = PartyVote(Option("PARTY2"), Option(780))
  val p23 = PartyVote(Option("PARTY2"), Option(790))
  val p24 = PartyVote(Option("PARTY2"), Option(1010))
  val p25 = PartyVote(Option("PARTY2"), Option(901))
  val p26 = PartyVote(Option("PARTY2"), Option(870))
  val p27 = PartyVote(Option("PARTY2"), Option(1200))
  val p28 = PartyVote(Option("PARTY2"), Option(1100))
  val p29 = PartyVote(Option("PARTY2"), Option(690))
  val p210 = PartyVote(Option("PARTY2"), Option(710))

  val p31 = PartyVote(Option("PARTY3"), Option(520))
  val p32 = PartyVote(Option("PARTY3"), Option(610))
  val p33 = PartyVote(Option("PARTY3"), Option(540))
  val p34 = PartyVote(Option("PARTY3"), Option(320))
  val p35 = PartyVote(Option("PARTY3"), Option(620))
  val p36 = PartyVote(Option("PARTY3"), Option(560))
  val p37 = PartyVote(Option("PARTY3"), Option(430))
  val p38 = PartyVote(Option("PARTY3"), Option(590))
  val p39 = PartyVote(Option("PARTY3"), Option(230))
  val p310 = PartyVote(Option("PARTY3"), Option(340))

  val p41 = PartyVote(Option("PARTY4"), Option(420))
  val p42 = PartyVote(Option("PARTY4"), Option(210))
  val p43 = PartyVote(Option("PARTY4"), Option(340))
  val p44 = PartyVote(Option("PARTY4"), Option(320))
  val p45 = PartyVote(Option("PARTY4"), Option(520))
  val p46 = PartyVote(Option("PARTY4"), Option(360))
  val p47 = PartyVote(Option("PARTY4"), Option(230))
  val p48 = PartyVote(Option("PARTY4"), Option(510))
  val p49 = PartyVote(Option("PARTY4"), Option(260))
  val p410 = PartyVote(Option("PARTY4"), Option(440))

  val p51 = PartyVote(Option("PARTY5"), Option(410))
  val p52 = PartyVote(Option("PARTY5"), Option(350))
  val p53 = PartyVote(Option("PARTY5"), Option(340))
  val p54 = PartyVote(Option("PARTY5"), Option(580))
  val p55 = PartyVote(Option("PARTY5"), Option(290))
  val p56 = PartyVote(Option("PARTY5"), Option(390))
  val p57 = PartyVote(Option("PARTY5"), Option(230))
  val p58 = PartyVote(Option("PARTY5"), Option(450))
  val p59 = PartyVote(Option("PARTY5"), Option(390))
  val p510 = PartyVote(Option("PARTY5"), Option(990))

  val pv1 = List(p11, p21, p31, p41, p51)
  val pv2 = List(p12, p22, p32, p42, p52)
  val pv3 = List(p13, p23, p33, p43, p53)
  val pv4 = List(p14, p24, p34, p44, p54)
  val pv5 = List(p15, p25, p35, p45, p55)
  val pv6 = List(p16, p26, p36, p46, p56)
  val pv7 = List(p17, p27, p37, p47, p57)
  val pv8 = List(p18, p28, p38, p48, p58)
  val pv9 = List(p19, p29, p39, p49, p59)
  val pv10 = List(p110, p210, p310, p410, p510)
  val partyVotes = List(pv1, pv2, pv3, pv4, pv5, pv6, pv7, pv8, pv9, pv10)

  def props() = Props(new VoteFlyActor)

  def readElectorate: List[Electorate] = {
    val stream = getClass.getResourceAsStream(s"/electorate.json")
    val json = scala.io.Source.fromInputStream(stream)
      .getLines
      .toList
      .mkString(" ")
      .trim
      .replaceAll("\\s+", " ")

    implicit val formal1: JsonFormat[Location] = jsonFormat2(Location)
    implicit val formal2: JsonFormat[Electorate] = jsonFormat8(Electorate)
    json.parseJson.convertTo[List[Electorate]]
  }

}

class VoteFlyActor extends Actor with AppConf with AppLogger {

  import VoteFlyActor._

  override def receive: Receive = {
    case VoteFly =>
      logger.debug(s"vote fly.. ")

      val sta = List(("Victoria", 21926, 56),
        ("New South Wales", 24585, 67),
        ("Tasmania", 235, 59),
        ("Queensland", 1977, 50),
        ("Northern Territory", 202, 54),
        ("South Australia", 871, 52),
        ("Western Australia", 1086, 50),
        ("Australia Capital Territory", 374, 64)
      )

      for(s <- sta) {
        val id = UUID.randomUUID().toString
        val vote = Vote(
          id,
          s._1,
          "",
          "",
          "",
          "",
          "",
          "",
          Location(2.1, 3.1),
          "2010",
          None,

          None,
          Option(s._2),
          Option(s._3)
        )

        CassandraStore.createVote(vote)
      }


    //      val partyVote = Random.shuffle(partyVotes).head
//
//      for (y <- years) {
//        for (e <- electorates) {
//          for (v <- partyVote) {
//            val id = UUID.randomUUID().toString
//            val vote = Vote(
//              id,
//              e.province,
//              e.district,
//              e.electorate,
//              e.asanaya,
//              e.kottasaya,
//              e.dsDivision,
//              e.gsDivision,
//              Location(e.location.lat, e.location.lon),
//              y,
//              DateFactory.formatToLocalDate(s"$y-01-01", DateFactory.DATE_FORMAT),
//
//              v.partyName,
//              v.partyVotes
//            )
//
//            CassandraStore.createVote(vote)
//          }
//        }
//      }

  }

}
