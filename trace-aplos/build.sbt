name := "trace-aplos"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  val akkaVersion       = "2.4.14"
  val akkaHttpVersion   = "10.1.4"

  Seq(
    "com.typesafe.akka"               %% "akka-actor"                     % akkaVersion,
    "com.typesafe.akka"               %% "akka-stream"                    % akkaVersion,
    "com.typesafe.akka"               %% "akka-slf4j"                     % akkaVersion,
    "io.spray"                        %% "spray-json"                     % "1.3.5",
    "com.lightbend.akka"              %% "akka-stream-alpakka-cassandra"  % "1.0-M1",
    "io.getquill"                     %% "quill-cassandra"        % "3.4.10",
    "com.typesafe.akka"               %% "akka-http"                      % akkaHttpVersion,
    "com.typesafe.akka"               %% "akka-http-spray-json"           % akkaHttpVersion,
    "org.elasticsearch.client"        % "transport"                       % "6.2.3",
    "org.json4s"                      %% "json4s-jackson"                 % "3.6.4",
    "redis.clients"                   % "jedis"                           % "2.4.2",
    "org.slf4j"                       % "slf4j-api"                       % "1.7.5",
    "io.github.dataoperandz"          % "cassper"                         % "0.4",
    "ch.qos.logback"                  % "logback-classic"                 % "1.0.9",
    "org.scalatest"                   % "scalatest_2.11"                  % "2.2.1"               % "test"
  )
}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".RSA" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".keys" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".logs" => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
